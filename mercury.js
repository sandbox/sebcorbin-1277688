(function($) {
  Drupal.behaviors.mercuryEditor = {
    attach: function (context, settings) {
      // Run only if mercury is required
      if (settings.mercury) {
        window.mercuryLoaded = function() {
          // Wait for iFrame to load
          $('.mercury-iframe').load(function() {
            for (id in settings.mercury.nodes) {
              // Retrieving container
              var container = $('.mercury-iframe').contents().find(id);

              // Setting events associated
              container.click(Drupal.behaviors.mercuryEditor.click);

              Mercury.bind('unfocus:regions', function() {
                if (mercuryInstance.visible) {
                  mercuryInstance.toggleInterface();
                  container.click(Drupal.behaviors.mercuryEditor.click);
                }
              });
            }
          });
        };
      }
    },

    click: function() {
      mercuryInstance.toggleInterface();
      $(this).unbind('click', Drupal.behaviors.mercuryEditor.click);
      $(this).focus();
    },

    save: function(id, html) {
      // Save node
      var body = Drupal.settings.mercury.nodes[id];
      $.ajax({
        type: "POST",
        url: Drupal.settings.basePath + 'mercury/save/node/' + body.nid,
        data: {body: html, lang: body.lang},
        success: function(obj) {
          if (obj.status == 'saved') {
            var element = '<div class="mercury-status">' + Drupal.t('%title has been saved.', {'%title': obj.title}) + '</div>';
            $(element).insertBefore($(obj.nid)).delay(1300).fadeOut(function () {
              $(this).remove();
            });
          }
        }
      });
    }
  };
}(jQuery));